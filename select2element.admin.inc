<?php

/**
 * @file
 * Admin page callbacks for the Select2 element module
 */

function select2element_admin_settings() {
  $form['select2element_paths'] = array(
    '#type' => 'textarea',
    '#title' => t('Paths where to allow Select2 element'),
    '#default_value' => variable_get('select2element_paths', "node/add/*\nnode/*/edit"),
    '#description' => t('Specify each path on a separate line. You can use * or %front tokens.', array('%front' => '<front>')),  
  );
  
  $form['select2element_elements'] = array(
    '#type' => 'textarea',
    '#title' => t('List of form elements where to allow Select2 element'),
    '#default_value' => variable_get('select2element_elements', "taxonomy"),
    '#description' => t('Specify each element on a separate line. Leave empty to process all form elements'),  
  );
  
  $form['select2element_allowed_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Allowed widget types'),
    '#default_value' => variable_get('select2element_allowed_types', array('select' => 'select', 'select_multiple' => 'select_multiple', 'autocomplete' => 'autocomplete')),
    '#options' => array('select' => 'Selectbox', 'select_multiple' => 'Multi-value selectbox', 'autocomplete' => 'Autocomplete field (tags, author)'), 
    '#description' => t('Choose allowed Select2 elements that will be used on form elements specified above. E.g. you can only use select2 widget for taxonomy tags.'),  
  );
  
  return system_settings_form($form);
}
